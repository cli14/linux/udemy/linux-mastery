## Linux(Ubuntu) Commnds

### Command Structure

```
> commandName options userInput
```

### To get locacation of commandName(eg java)

```
1. 
> echo $PATH
-- to get list of dir where linux look for command binaries
-- dir are separated using colon (:)
-- linux read from left to right
-- which ever dir contains binaries for commandName first that command will be executed

2. 
which <commandName>
-- eg. which java
-- it return the dir among $PATH which contains binaries for <commandName>

which -a <commandName>
-- eg. which -a java
-- it return ALL the dir among $PATH which contains binaries for <commandName>
```

### Linux Manual

```
1. man - its short form for manual, its used to find manual for a specific linux command
> man -k <commandName>
-- eg. man -k which
-- this wil give section (out of 8 section) of <commandName> manual
-- here -k means it will search whole manual for the specified <commandName>

> man <sectionIndex> <commandName>
-- eg. man 1 which

```
### Linux Command

```
1. echo 
>  echo "Hello"

2. cal -- to get calendar
> cal 
-- it give calendar of current month
> cal 2020 
-- it give calendar of specified year
> cal -y 
-- it give calendar of current year

3. date -- to get calendar
> date 
-- it give the current date and time

4. clear  -- to clear terminal
> clear or (ctrl+L)

5. history  -- to get past comands
> history
Note: 
  - to run a past command from hsitory - !<command-index>
  - to run most recent command - !!

> history -c; history -w 
-- it will clear the history
-- here c means clear, w means write 

6. exit  -- to close the terminal
-- shortcut - ctrl+d

7. cat - conCATenation
cat <file1> <file2> ...
- it returns combine conte

8. sort -r
- to sort STDOUT in some order
- this is generaly used with pipe

```

#### Linux Command Redirection(Data-Stream)

```
Data-Stream has number associate with it as shown below

1 means standard-output (STDOUT)
2 means standard-error(STDERR)
0 means standard-input(STDIN)

- 1 means standard-output
eg 1.
> cat 1> output.txt (or cat > output.txt )
- it will redirect standard-output to a file
- whenever we run "commandName 1> <file>" - it will remove(or truncate) all the content of <file> and start fresh
- in order to preserve (or append ) standard-output use (>>)  as below command
eg 2.
>cat 1>> output.txt (or cat >> output.txt)

- 2 means standard-error

eg1 .
> cat -k blah 2> error.txt -- here "cat -k blah" is invalid command which gives error
- it will redirect standard-error to a file
- whenever we run "invalid-command 2> <file>" - it will remove(or truncate) all the content of <file> and start fresh
- in order to preserve (or append ) standard-error use (>>)  as below command

eg2 .
> cat -k blah 2>> error.txt -- here "cat -k blah" is invalid command which gives error

[IMP] - combining standard-output, standard-error
> cat 1>> outputt.txt 2>> errorr.txt
- as per the response it will redirect to
  -- outputt.txt on SUCCESS
  -- errorr.txt ON ERROR  

- 0 means standard-input

eg 1.
> cat 0< input.txt (or cat < input.txt )
- it will redirect standard-input from terminal to flile

[IMP] - combining standard-output, standard-input

> cat 0< input.txt 1> output.txt
```

### Linux Command Pipeline

```
1. Pipe (i.e |)
commandName1 options args | commandName2 options args | commandName3 options args  | ...
- it is a way of passing STDOUT of one command as STDIN on another command.
eg 1. 
date | cut -d " " -f 1
  -- this will take out of "date" command and consider as input for "cut" command
  
eg 2. - combining pipe to redirection 
date | cut -d " " -f 1 1> today.txt

2. Pipe with "tee" command
- with the help of tee command we 
  -- save standard-output to a file as well as
  -- pass the same output-stream to terminal
    --- eg 1. date | tee today.txt
	  ---- it will save date output to a file as well as print the same output to terminal
    --- eg 2. date | tee fulldate.txt | cut -d " " -f 1 1> today.txt - this works but without tee i.e. date 1> fulldate.txt | cut -d " " -f 1 1> today.txt will not work
	  ---- without tee, once we done redirection then we can't continue piping
	  ---- with tee, once we done redirection using tee then also we can continue piping
	  
2. Pipe with "xargs" command
- if we do 
date | echo 
- it will print empty because echo does not take STDIN like "cut" (definition of pipe - it is a way of passing STOOUT of one command as STDIN on another command.)
- to get reponse from previous command i.e STDOUT to next command as STDIN who does not take standard input we use "xargs" as shown below
date | xargs echo

eg 1.
without args
date | cut -d " " -f 1 | echo -- empty response
with args
date | cut -d " " -f 1 | xargs echo -- return response as day of date

```

### Linux alias
```
- to give shortname for a very long command we use alias

1. to get list of aliases
> alias

2. to create new alias
- refer - https://www.tecmint.com/create-alias-in-linux/
- syntax - alias aliasName="commands with(out) pipe" 
3. aliases can be used in piplene as well
eg 1.
getdates | xargs echo new-pipeline

```

### Linux File System

```
1. Root dir
cd / -- it is root dir

2. Home dir - it's dir of all users its like %USERPROFILE% in windows
 - 2.1 - cd ~ (~ is called tilda its shortcut for logged-in user dir)
   -- it takes us to user hom dir (i.e %USERPROFILE%)
   
3. root user - its like admin user in Windows
  - home dir of root user is /root 
  
4. Naviagating accross dir
> pwd
- return present working dir

>ls ( or dir) 
 - its short form of "list"
 - its list content of pwd

> ls <dir> (or dir <dir>)
 - its list content of <dir>
 
> cd -- change dir
 - its used to change dir
 
 ABSOLUTE PATH - path start from root dir
 - cd /<dir>
   -- to move to the root dir
   -- this is cd using ABSOLUTE path
   -- using above command we can move to any dir
   eg 1.
     --- cd /home
   eg 2.
     --- cd /opt
   eg 3. 
     --- cd ~/downloads
	   ---- to move to /home/<user>/downloads
  
  RELATIVE PATH - path start from cureent dir
  
  - cd <dir> 
    -- here we dont have / prefix

> use of tab / tab completion
Single Tab
- pressing tab auto complete dir and file name for us

Double Tap
- pressing list of all conflicting options for auto complete dir and file name for us

```

#### Refrences
```
s4L19
- https://www.cyberciti.biz/tips/understanding-unixlinux-file-system-part-i.html
- https://askubuntu.com/questions/94734/what-is-the-templates-folder-in-the-home-directory-for
```

### Linux Wildcard

```
1. ">"
- this is standard-output redirection
- its also used to create a file in linux
  -- eg1
  "cat > <file>"
  "echo "hello" > <file>"
- whenever we run "commandName > <file>" - it will remove(or truncate) all the content of <file> and start fresh
- in order to preserve (or append ) standard-error use (>>)  as below command  

2. ">>"
- echo "hello" >> <file>

3. Pipe (i.e |)
- its used to take output stram from one command as in put stream to another command
eg. date | cut -d " " -f 1
  -- this will take out of "date" command and consider as input for "cut" command

4. Tilda(~)
~ is called tilda 
its shortcut for logged-in user dir

5. Dot(.)
- this dir
eg.

cd .
- it tell cd to same dir

6. Double Dot(..)
- it tell parent dir
- if we have /parent/child1/child2 and we are in cild2 dir
  -- then "cd ."  will keep us in child2 dir
  -- and "cd .." will move us to child1/child2
  -- and "cd ../.." will us to parent dir


```

### Notes

```
1. To get previous command
Way1 - Up and down arrow keys
```


# Asignment 
```
secton 2- 
task1
ls -l /etc 1> file1.txt
ls -l /etc 1> file2.txt

task2
cat file1.txt file2.txt | tee  unsorted.txt | sort -r 1> reverse.txt
```